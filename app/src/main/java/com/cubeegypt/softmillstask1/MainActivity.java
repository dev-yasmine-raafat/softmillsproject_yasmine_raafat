package com.cubeegypt.softmillstask1;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.cubeegypt.softmillstask1.models.PhotoModel;
import com.cubeegypt.softmillstask1.viewmodels.PhotosViewModel;
import com.cubeegypt.softmillstask1.views.PhotosRecyclerAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.searchView)
    SearchView searchView;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.tvResultCount)
    TextView tvResultCount;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private PhotosRecyclerAdapter adapter;
    private PhotosViewModel photosViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        adapter = new PhotosRecyclerAdapter(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));
        recyclerView.setAdapter(adapter);

        photosViewModel = ViewModelProviders.of(this).get(PhotosViewModel.class);
        photosViewModel.getPhotos().observe(this, new Observer<List<PhotoModel>>() {
            @Override
            public void onChanged(@Nullable List<PhotoModel> photoModels) {
                Log.e("onChanged","data change");
                tvResultCount.setText(photoModels.size()+" "+getString(R.string.text_results));

                if (photoModels.size() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    tvMsg.setVisibility(View.GONE);
                    adapter.addData(photoModels);
                }else{
                    recyclerView.setVisibility(View.GONE);
                    tvMsg.setVisibility(View.VISIBLE);
                }
            }
        });

        searchView.setOnQueryTextListener(this);
    }
    @OnClick(R.id.fBtnUpload)
    void upload(){
        startActivity(new Intent(this,UploadActivity.class));
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        photosViewModel.searchPhotos(newText);
        return false;
    }
}
