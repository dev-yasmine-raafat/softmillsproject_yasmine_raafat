package com.cubeegypt.softmillstask1.network;

import android.content.Context;
import android.util.Log;

import com.cubeegypt.softmillstask1.R;
import com.cubeegypt.softmillstask1.components.Toaster;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class ServiceUtils {

    public static void handleException(Throwable t) {
        if (t instanceof SocketTimeoutException)
            Log.e("SocketTimeoutException","timeout");
        else if (t instanceof UnknownHostException)
            Log.e("UnknownHostException","internet");

        else if (t instanceof ConnectException)
            Log.e("ConnectException","internet");
        else
            Log.e("Unknown",t.getClass().getName());

    }

    public static void makeToast(Context context, int msgId) {
        Toaster toaster = new Toaster(context);
        toaster.makeToast(context.getString(msgId));

    }
}
