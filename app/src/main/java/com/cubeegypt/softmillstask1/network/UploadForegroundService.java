package com.cubeegypt.softmillstask1.network;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.cubeegypt.softmillstask1.R;
import com.cubeegypt.softmillstask1.repositories.UploadRepository;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class UploadForegroundService extends Service implements UploadListener {

    private File f;
    private UploadRepository repository;
    private String CHANNEL_ID ;
    private String CHANNEL_NAME ;

    @Override
    public void onCreate() {
        super.onCreate();

        CHANNEL_ID = getString(R.string.app_name);
        CHANNEL_NAME = getString(R.string.app_name);
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build();

            startForeground(1, notification);
        }
        repository = new UploadRepository(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        convertDrawableToFile(R.drawable.cat);

        if (f != null) {
            createNotification(getString(R.string.uploading));
            repository.upload(f);

        }else{
            createNotification("No file");
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stop();
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private void createNotification(String msg){


        NotificationManager notification = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder mbuilder = new NotificationCompat.Builder(this,CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(msg))
                .setAutoCancel(true).setSound(RingtoneManager.getActualDefaultRingtoneUri(this,
                        RingtoneManager.TYPE_NOTIFICATION))
                .setContentText(msg);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH);
            // Configure the notification channel.
            notification.createNotificationChannel(mChannel);
        } else {
            mbuilder.setContentTitle(getString(R.string.app_name))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                    .setVibrate(new long[]{100, 250})
                    .setAutoCancel(true);
        }
        notification.notify(1, mbuilder.build());
    }

    private void convertDrawableToFile(int id){
        try
        {
            f = new File(getCacheDir() + File.separator + "cat_image");
            InputStream is = getResources().openRawResource(id);
            OutputStream out = new FileOutputStream(f);

            int bytesRead;
            byte[] buffer = new byte[1024];
            while((bytesRead = is.read(buffer)) > 0)
            {
                out.write(buffer, 0, bytesRead);
            }

            out.close();
            is.close();
        }
        catch (IOException ex)
        {
            f = null;
        }
    }
    @Override
    public void success() {

        createNotification(getString(R.string.uploaded));

        stop();



    }

    @Override
    public void error(String string) {

        createNotification(string);

        stop();

    }

    @Override
    public void failure(Throwable t) {

        createNotification(getString(R.string.upload_failure));

        stop();


    }

    private void stop(){
        if (Build.VERSION.SDK_INT >= 26) {
            stopForeground(STOP_FOREGROUND_DETACH);
        }else {

            stopSelf();

        }
    }
}
