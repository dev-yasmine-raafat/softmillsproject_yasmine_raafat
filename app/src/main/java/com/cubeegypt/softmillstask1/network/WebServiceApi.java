package com.cubeegypt.softmillstask1.network;



import com.cubeegypt.softmillstask1.models.ResponseModel;
import com.cubeegypt.softmillstask1.models.UploadResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface WebServiceApi {

    @FormUrlEncoded
    @POST(Urls.SEARCH_PHOTOS)
    Call<ResponseModel> SEARCH_PHOTOS(@Header(ServiceConstants.LANG_KEY) String lang,
                                      @Header(ServiceConstants.AUTH_KEY) String auth,
                                      @Header(ServiceConstants.USER_TYPE_KEY) String userType,
                                      @Field(ServiceConstants.KEYWORD) String keyword);

    @Multipart
    @POST(Urls.UPLOAD_PHOTO)
    Call<UploadResponse> UPLOAD_PHOTO(@Header(ServiceConstants.LANG_KEY) String lang,
                                      @Header(ServiceConstants.AUTH_KEY) String auth,
                                      @Header(ServiceConstants.USER_TYPE_KEY) String userType,
                                      @Part MultipartBody.Part file);
}
