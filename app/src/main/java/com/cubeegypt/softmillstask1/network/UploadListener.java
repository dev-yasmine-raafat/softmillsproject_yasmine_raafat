package com.cubeegypt.softmillstask1.network;

public interface UploadListener {

    void success();

    void error(String string);

    void failure(Throwable t);
}
