package com.cubeegypt.softmillstask1.network;

public class ServiceConstants {
    public static final String LANG_KEY = "x-lang-code";
    public static final String LANG_VALUE = "en-us";

    public static final String AUTH_KEY = "x-auth-token";
    public static final String AUTH_VALUE = "1bc8f75619cf033d8eae40af6a9c2a5e";
    public static final String AUTH_UPLOAD_VALUE = "e7cde7086fb857a39e5fb4e5567eecb0";

    public static final String USER_TYPE_KEY = "x-user-type";
    public static final String USER_TYPE_VALUE = "1";
    public static final String USER_TYPE_VALUE_0 = "0";

    public static final String KEYWORD = "keyword";

    public static final String IMAGE_KEY = "image_profile";
}
