package com.cubeegypt.softmillstask1.network;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.media.RingtoneManager;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.cubeegypt.softmillstask1.R;
import com.cubeegypt.softmillstask1.repositories.UploadRepository;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class UploadBackgroundService extends IntentService implements UploadListener{

    private final UploadRepository repository;
    private File f;
    public UploadBackgroundService() {
        super("UploadBackgroundService");
        repository = new UploadRepository(this);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        convertDrawableToFile(R.drawable.cat);

        if (f != null) {
            createNotification(getString(R.string.uploading));
            repository.upload(f);

        }else{
            createNotification("null");
        }

    }

    private void createNotification(String msg){
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
//        Notification notification = new Notification(/* your notification */);
//
//        notificationManager.notify(0, notification);


        NotificationManager notification = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder mbuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setStyle(new NotificationCompat.BigTextStyle()
                .bigText(msg))
                .setAutoCancel(true).setSound(RingtoneManager.getActualDefaultRingtoneUri(this,
                        RingtoneManager.TYPE_NOTIFICATION)).setContentTitle(getString(R.string.app_name))
                .setContentText(msg);

        notification.notify(0, mbuilder.build());
    }

    private void convertDrawableToFile(int id){
        try
        {
            f = new File(getCacheDir() + File.separator + "cat_image");
            InputStream is = getResources().openRawResource(id);
            OutputStream out = new FileOutputStream(f);

            int bytesRead;
            byte[] buffer = new byte[1024];
            while((bytesRead = is.read(buffer)) > 0)
            {
                out.write(buffer, 0, bytesRead);
            }

            out.close();
            is.close();
        }
        catch (IOException ex)
        {
            f = null;
        }
    }


    @Override
    public void success() {

        createNotification(getString(R.string.uploaded));

    }

    @Override
    public void error(String string) {

        createNotification(string);

    }

    @Override
    public void failure(Throwable t) {

        createNotification(getString(R.string.upload_failure));


    }
}
