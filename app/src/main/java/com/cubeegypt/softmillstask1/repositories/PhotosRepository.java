package com.cubeegypt.softmillstask1.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;


import com.cubeegypt.softmillstask1.models.PhotoModel;
import com.cubeegypt.softmillstask1.models.ResponseModel;
import com.cubeegypt.softmillstask1.network.ServiceConstants;
import com.cubeegypt.softmillstask1.network.WebServiceApi;
import com.cubeegypt.softmillstask1.network.WebServiceClient;
import com.cubeegypt.softmillstask1.viewmodels.PhotosResponseListener;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotosRepository implements Callback<ResponseModel> {

    private WebServiceApi api;

    private PhotosResponseListener listener;

    public PhotosRepository(PhotosResponseListener listener) {

        this.listener = listener;

        api = WebServiceClient.getClient().create(WebServiceApi.class);

        Log.e("PhotosRepository","constructor");
    }

    public void getPhotos(String query) {

        Log.e("PhotosRepository","api call");
        final MutableLiveData<List<PhotoModel>> responseModelMutableLiveData =
                new MutableLiveData<>();
        Call<ResponseModel> call = api.SEARCH_PHOTOS(ServiceConstants.LANG_VALUE,
                ServiceConstants.AUTH_VALUE, ServiceConstants.USER_TYPE_VALUE,query);

        call.enqueue(this);

        Log.e("PhotosRepository","return");


//        return responseModelMutableLiveData;
//
//
//        new Callback<ResponseModel>() {
//            @Override
//            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
//
//                Log.e("PhotosRepository","api response");
//
//                if (response.isSuccessful()){
//                    Log.e("PhotosRepository","success");
//                    Log.e("PhotosRepository","size "+response.body().getData().getData().size());
//
//                    responseModelMutableLiveData.setValue(response.body().getData().getData());
////                    listener.setData(responseModelMutableLiveData);
//
//                }else {
//                    try {
//                        listener.errorMessage(response.errorBody().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseModel> call, Throwable t) {
//                Log.e("PhotosRepository","api fail");
//
//                listener.failure(t);
//            }
//        }
    }

    @retrofit2.internal.EverythingIsNonNull
    @Override
    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                new MutableLiveData<>();
        Log.e("PhotosRepository","api response");

        if (response.isSuccessful()){
            listener.setData(response.body().getData().getData());

        }else {
            try {
                listener.errorMessage(response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @retrofit2.internal.EverythingIsNonNull
    @Override
    public void onFailure(Call<ResponseModel> call, Throwable t) {
        Log.e("PhotosRepository","api fail");

        listener.failure(t);

    }
}
