package com.cubeegypt.softmillstask1.repositories;

import com.cubeegypt.softmillstask1.models.UploadResponse;
import com.cubeegypt.softmillstask1.network.ServiceConstants;
import com.cubeegypt.softmillstask1.network.UploadListener;
import com.cubeegypt.softmillstask1.network.WebServiceApi;
import com.cubeegypt.softmillstask1.network.WebServiceClient;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadRepository implements Callback<UploadResponse> {

    private final UploadListener listener;
    private final WebServiceApi api;

    public UploadRepository(UploadListener listener){
        api = WebServiceClient.getClient().create(WebServiceApi.class);
        this.listener = listener;
    }

    public void upload(File file){
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("image"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData(ServiceConstants.IMAGE_KEY, file.getName(), requestFile);

        Call<UploadResponse> call = api.UPLOAD_PHOTO(ServiceConstants.LANG_VALUE,
                ServiceConstants.AUTH_UPLOAD_VALUE, ServiceConstants.USER_TYPE_VALUE_0,body);

        call.enqueue(this);

    }

    @Override
    public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response) {

        if (response.isSuccessful())
            listener.success();
        else {
            try {
                listener.error(response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onFailure(Call<UploadResponse> call, Throwable t) {

        listener.failure(t);

    }
}
