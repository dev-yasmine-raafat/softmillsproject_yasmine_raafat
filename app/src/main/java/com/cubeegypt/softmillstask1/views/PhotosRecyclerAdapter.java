package com.cubeegypt.softmillstask1.views;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cubeegypt.softmillstask1.R;
import com.cubeegypt.softmillstask1.models.PhotoModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PhotosRecyclerAdapter extends RecyclerView.Adapter<PhotosRecyclerAdapter.ViewHolder> {


    private final Context context;
    private final LayoutInflater inflater;
    private List<PhotoModel> photosList;

    public PhotosRecyclerAdapter(Context context) {

        this.context = context;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        photosList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list_item_photo_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Log.e("onBindViewHolder", position + "");
        PhotoModel photoModel = photosList.get(position);
        Log.e("onBindViewHolder", photoModel.getCaption());


        if (photoModel.getPhotographer().getFullName() != null)
            holder.tvName.setText(photoModel.getPhotographer().getFullName());

        if (photoModel.getPhotographer().getUserName() != null)
            holder.tvUsername.setText("@" + photoModel.getPhotographer().getUserName());

//        holder.tvFollowers.setText(photoModel.getCaption());
        Picasso.with(context).load(photoModel.getUrl()).error(R.drawable.not_found)
                .into(holder.cIvImage);
//
    }

    @Override
    public int getItemCount() {
        return photosList.size();
    }

    public void addData(List<PhotoModel> results) {

        Log.e("addData", results.size() + "");

        this.photosList.clear();
        this.photosList.addAll(results);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cIvImage)
        CircleImageView cIvImage;

        @BindView(R.id.tvName)
        TextView tvName;

        @BindView(R.id.tvUsername)
        TextView tvUsername;

        @BindView(R.id.tvFollowing)
        TextView tvFollowing;

        @BindView(R.id.tvFollowers)
        TextView tvFollowers;

        @BindView(R.id.tvPhotoCount)
        TextView tvPhotoCount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
