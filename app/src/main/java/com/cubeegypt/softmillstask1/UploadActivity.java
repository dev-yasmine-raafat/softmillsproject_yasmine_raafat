
package com.cubeegypt.softmillstask1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.cubeegypt.softmillstask1.network.UploadBackgroundService;
import com.cubeegypt.softmillstask1.network.UploadForegroundService;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class UploadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.btnUpload)
    void upload() {
        if (!isMyServiceRunning()) {

            startService();
//            startService(new Intent(this, UploadBackgroundService.class));
        }
    }
    @OnClick(R.id.btnSkip)
    void stopUpload() {
        if (isMyServiceRunning()) {

            stopService();
//            startService(new Intent(this, UploadBackgroundService.class));
        }
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//            if ("com.cubeegypt.softmillstask1.network.UploadBackgroundService"
//                    .equals(service.service.getClassName())) {

            if ("com.cubeegypt.softmillstask1.network.UploadForegroundService"
                    .equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void startService() {
        Intent serviceIntent = new Intent(this, UploadForegroundService.class);
        ContextCompat.startForegroundService(this, serviceIntent);

    }

    public void stopService() {
        Intent serviceIntent = new Intent(this, UploadForegroundService.class);
        stopService(serviceIntent);
    }
}
