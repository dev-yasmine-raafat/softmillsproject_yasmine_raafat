package com.cubeegypt.softmillstask1.viewmodels;

import androidx.lifecycle.MutableLiveData;

import com.cubeegypt.softmillstask1.models.PhotoModel;
import com.cubeegypt.softmillstask1.models.ResponseModel;

import java.util.List;


public interface PhotosResponseListener {
    void setData(List<PhotoModel> photoModels);

    void errorMessage(String msg);

    void failure(Throwable t);
}
