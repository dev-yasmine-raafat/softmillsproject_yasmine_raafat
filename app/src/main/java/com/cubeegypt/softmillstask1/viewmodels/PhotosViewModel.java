package com.cubeegypt.softmillstask1.viewmodels;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.cubeegypt.softmillstask1.models.PhotoModel;
import com.cubeegypt.softmillstask1.models.ResponseModel;
import com.cubeegypt.softmillstask1.network.ServiceUtils;
import com.cubeegypt.softmillstask1.repositories.PhotosRepository;

import java.util.List;


public class PhotosViewModel extends ViewModel implements PhotosResponseListener {

    private MutableLiveData<ResponseModel> data = new MutableLiveData<>();
    private MutableLiveData<List<PhotoModel>> photos = new MutableLiveData<>();
    private MutableLiveData<String> errorMessage;
    private MutableLiveData<Throwable> throwable;

    private PhotosRepository photosRepository;

    private PhotosViewModel instance;

    public PhotosViewModel() {
        Log.e("PhotosViewModel","constructor");
        photosRepository = new PhotosRepository(this);
    }


//    public void init() {
//        Log.e("PhotosViewModel","init");
//        photosRepository = new PhotosRepository(this);
//        data = photosRepository.getPhotos("");
//
//    }

    public MutableLiveData<List<PhotoModel>> getPhotos() {
        Log.e("PhotosViewModel","getPhotos");
        return this.photos;
    }

    @Override
    public void setData(List<PhotoModel> photos) {
        Log.e("PhotosViewModel","setData");

        this.photos.postValue(photos);
    }

    @Override
    public void errorMessage(String msg) {
        Log.e("PhotosViewModel","error");
    }

    @Override
    public void failure(Throwable t) {
        Log.e("PhotosViewModel","failure");
        ServiceUtils.handleException(t);
    }

    public void searchPhotos(String query) {
        Log.e("PhotosViewModel","searchPhotos by "+query);

        photosRepository.getPhotos(query);
    }
}
