package com.cubeegypt.softmillstask1.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PhotoModel {

    @SerializedName("extension")
    @Expose
    private String extension;
    @SerializedName("sell_exclusive_price")
    @Expose
    private Integer sellExclusivePrice;
    @SerializedName("is_exclusive")
    @Expose
    private Boolean isExclusive;
    @SerializedName("caption")
    @Expose
    private String caption;
    @SerializedName("url_preview")
    @Expose
    private String urlPreview;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("sell_normal_price")
    @Expose
    private Integer sellNormalPrice;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("photographer")
    @Expose
    private PhotographerModel photographer;
    @SerializedName("is_saved")
    @Expose
    private Boolean isSaved;
    @SerializedName("is_liked")
    @Expose
    private Boolean isLiked;
    @SerializedName("is_rated")
    @Expose
    private Boolean isRated;
    @SerializedName("is_cart")
    @Expose
    private Boolean isCart;
    @SerializedName("is_purchased")
    @Expose
    private Boolean isPurchased;
    @SerializedName("can_purchase")
    @Expose
    private Boolean canPurchase;
    @SerializedName("tags")
    @Expose
    private List<TagModel> tags = null;
    @SerializedName("filters")
    @Expose
    private List<String> filters = null;
    @SerializedName("light_box")
    @Expose
    private List<Object> lightBox = null;
    @SerializedName("comment")
    @Expose
    private List<CommentModel> comment = null;
    @SerializedName("business_purchases")
    @Expose
    private List<BusinessPurchase> businessPurchases = null;
    @SerializedName("photographer_level")
    @Expose
    private Integer photographerLevel;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Integer getSellExclusivePrice() {
        return sellExclusivePrice;
    }

    public void setSellExclusivePrice(Integer sellExclusivePrice) {
        this.sellExclusivePrice = sellExclusivePrice;
    }

    public Boolean getIsExclusive() {
        return isExclusive;
    }

    public void setIsExclusive(Boolean isExclusive) {
        this.isExclusive = isExclusive;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getUrlPreview() {
        return urlPreview;
    }

    public void setUrlPreview(String urlPreview) {
        this.urlPreview = urlPreview;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getSellNormalPrice() {
        return sellNormalPrice;
    }

    public void setSellNormalPrice(Integer sellNormalPrice) {
        this.sellNormalPrice = sellNormalPrice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PhotographerModel getPhotographer() {
        return photographer;
    }

    public void setPhotographer(PhotographerModel photographer) {
        this.photographer = photographer;
    }

    public Boolean getIsSaved() {
        return isSaved;
    }

    public void setIsSaved(Boolean isSaved) {
        this.isSaved = isSaved;
    }

    public Boolean getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Boolean isLiked) {
        this.isLiked = isLiked;
    }

    public Boolean getIsRated() {
        return isRated;
    }

    public void setIsRated(Boolean isRated) {
        this.isRated = isRated;
    }

    public Boolean getIsCart() {
        return isCart;
    }

    public void setIsCart(Boolean isCart) {
        this.isCart = isCart;
    }

    public Boolean getIsPurchased() {
        return isPurchased;
    }

    public void setIsPurchased(Boolean isPurchased) {
        this.isPurchased = isPurchased;
    }

    public Boolean getCanPurchase() {
        return canPurchase;
    }

    public void setCanPurchase(Boolean canPurchase) {
        this.canPurchase = canPurchase;
    }

    public List<TagModel> getTags() {
        return tags;
    }

    public void setTags(List<TagModel> tags) {
        this.tags = tags;
    }

    public List<String> getFilters() {
        return filters;
    }

    public void setFilters(List<String> filters) {
        this.filters = filters;
    }

    public List<Object> getLightBox() {
        return lightBox;
    }

    public void setLightBox(List<Object> lightBox) {
        this.lightBox = lightBox;
    }

    public List<CommentModel> getComment() {
        return comment;
    }

    public void setComment(List<CommentModel> comment) {
        this.comment = comment;
    }

    public List<BusinessPurchase> getBusinessPurchases() {
        return businessPurchases;
    }

    public void setBusinessPurchases(List<BusinessPurchase> businessPurchases) {
        this.businessPurchases = businessPurchases;
    }

    public Integer getPhotographerLevel() {
        return photographerLevel;
    }

    public void setPhotographerLevel(Integer photographerLevel) {
        this.photographerLevel = photographerLevel;
    }

}
