package com.cubeegypt.softmillstask1.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessPurchase {

    @SerializedName("is_phone_verified")
    @Expose
    private Boolean isPhoneVerified;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("name_ar")
    @Expose
    private String nameAr;
    @SerializedName("is_email_verified")
    @Expose
    private Boolean isEmailVerified;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("brand_data_updates")
    @Expose
    private Boolean brandDataUpdates;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("brand_thumbnail")
    @Expose
    private String brandThumbnail;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("brand_phone")
    @Expose
    private String brandPhone;
    @SerializedName("brand_address")
    @Expose
    private String brandAddress;
    @SerializedName("image_cover")
    @Expose
    private String imageCover;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("brand_image_cover")
    @Expose
    private String brandImageCover;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("brand_status")
    @Expose
    private Integer brandStatus;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("hash")
    @Expose
    private String hash;
    @SerializedName("name_en")
    @Expose
    private String nameEn;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("is_brand")
    @Expose
    private Boolean isBrand;
    @SerializedName("is_brand_text")
    @Expose
    private String isBrandText;
    @SerializedName("has_brand_update")
    @Expose
    private Boolean hasBrandUpdate;
    @SerializedName("image_profile")
    @Expose
    private String imageProfile;

    public Boolean getIsPhoneVerified() {
        return isPhoneVerified;
    }

    public void setIsPhoneVerified(Boolean isPhoneVerified) {
        this.isPhoneVerified = isPhoneVerified;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public Boolean getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(Boolean isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getBrandDataUpdates() {
        return brandDataUpdates;
    }

    public void setBrandDataUpdates(Boolean brandDataUpdates) {
        this.brandDataUpdates = brandDataUpdates;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getBrandThumbnail() {
        return brandThumbnail;
    }

    public void setBrandThumbnail(String brandThumbnail) {
        this.brandThumbnail = brandThumbnail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBrandPhone() {
        return brandPhone;
    }

    public void setBrandPhone(String brandPhone) {
        this.brandPhone = brandPhone;
    }

    public String getBrandAddress() {
        return brandAddress;
    }

    public void setBrandAddress(String brandAddress) {
        this.brandAddress = brandAddress;
    }

    public String getImageCover() {
        return imageCover;
    }

    public void setImageCover(String imageCover) {
        this.imageCover = imageCover;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getBrandImageCover() {
        return brandImageCover;
    }

    public void setBrandImageCover(String brandImageCover) {
        this.brandImageCover = brandImageCover;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getBrandStatus() {
        return brandStatus;
    }

    public void setBrandStatus(Integer brandStatus) {
        this.brandStatus = brandStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Boolean getIsBrand() {
        return isBrand;
    }

    public void setIsBrand(Boolean isBrand) {
        this.isBrand = isBrand;
    }

    public String getIsBrandText() {
        return isBrandText;
    }

    public void setIsBrandText(String isBrandText) {
        this.isBrandText = isBrandText;
    }

    public Boolean getHasBrandUpdate() {
        return hasBrandUpdate;
    }

    public void setHasBrandUpdate(Boolean hasBrandUpdate) {
        this.hasBrandUpdate = hasBrandUpdate;
    }

    public String getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(String imageProfile) {
        this.imageProfile = imageProfile;
    }

}
